package org.practice;

public class Runner extends Telephone {

	@Override
	public void gearchange() {
		System.out.println("gearchange");

	}

	@Override
	public void music() {
		System.out.println("music");
	}

	@Override
	public void with() {
		System.out.println("with");
	}

	@Override
	public void lift() {
		System.out.println("lift");

	}

	@Override
	public void disconect() {
		System.out.println("disconect");

	}

	public static void main(String[] args) {
		Runner r = new Runner();
		r.gearchange();
		r.music();
		r.with();
		r.lift();
		r.disconect();
		Car c = new Car();
		c.drive();
		c.Stop();
	}

}

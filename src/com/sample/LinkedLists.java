package com.sample;

import java.util.LinkedList;
import java.util.List;

public class LinkedLists {
	
	public static void main(String[] args) {
		List<String> l = new LinkedList<>();
		l.add("karthi");
		l.add("Vicky");
		l.add("sathish");
		l.add("kamal");
		l.add("Akash");
		l.add("sathish");
		l.add("Akash");
		System.out.println(l);
		List<Integer> ex = new LinkedList<Integer>();
		ex.add(15);
		ex.add(12);
		ex.add(24);
		ex.add(15);
		ex.add(10);
		ex.add(12);
		ex.add(null);
		System.out.println(ex);
		System.out.println("linked list allow the dublicate and null values");
	}

}

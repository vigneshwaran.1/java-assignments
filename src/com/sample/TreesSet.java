package com.sample;

import java.util.Set;
import java.util.TreeSet;

public class TreesSet {
	
	public static void main(String[] args) {
		Set<Integer> set = new TreeSet<>();
		set.add(15);
		set.add(10);
		set.add(35);
		set.add(15);
		set.add(20);
		set.add(10);
		System.out.println(set);
		boolean remove = set.remove(10);
		System.out.println(set);
		set.add(10);
		boolean contains = set.contains(10);
		System.out.println(contains);
		System.out.println(set);
		int size = set.size();
		System.out.println(size);
		System.out.println("Duplicate values not allowed by Treeset");
	}

}

package com.sample;

public class StringConcept {
	
	public static void main(String[] args) {

		String str = "Hello";
		String str1 = "Hello";
		String str2 = new String("Hello");
		String str3 = "java";
		boolean a = str.equals(str1);
		boolean b = str.equals(str2);
		boolean c = str.equals(str3);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}


}

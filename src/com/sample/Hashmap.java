package com.sample;

import java.util.HashMap;

public class Hashmap {
	
	public static void main(String[] args) {
		HashMap<String, Integer> has = new HashMap<>();
		has.put("Vigneshwaran", 10);
		has.put("karthi", 70);
		has.put("Logu", 130);
		has.put("Visu", 109);
		has.put("Suresh", 13);
		System.out.println(has);
		Integer integer = has.get("Vigneshwaran");
		String string = integer.toString();
		System.out.println(string);
		Integer remove = has.remove("Logu");
		System.out.println(has);
		
	}

}

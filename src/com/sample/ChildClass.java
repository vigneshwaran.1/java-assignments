package com.sample;

public class ChildClass extends ParentClass{
	
	public void properties3() {
		System.out.println("property 3 belongs to childclass");

	}

	public static void main(String[] args) {
		GrandParent g = new GrandParent();// grant parent
		g.properite1();

		ParentClass p = new ParentClass();// parent
		p.properties2();

		ChildClass c = new ChildClass();// child
		c.properties3();

	}

}
